var path = require("path");

var cluster= require("cluster")
var fs = require("fs")
var net = require ( "net")

var config = {

  node: {
    fs: "empty",
    cluster: "empty",
    net: "empty"
  },
  entry: ["./src/app.js"],
  output: {
    path: path.resolve(__dirname, "build"),
    filename: "bundle.js"
  },
  resolve: {
    extensions: [".ts", ".tsx", ".js"]
  },
  module: {
    rules: [
        {
            test: /\.(js)$/,
            include: path.resolve(__dirname, 'src'),
            loader: 'babel-loader',
            exclude: /node_modules/,
            query: {
              presets:[ 'es2015'],
            }
        },
        {
            test:/\.css$/,
            include: path.resolve(__dirname, 'src/lib/views/raiz'),
            loader:['style', 'css']
        },
        {
          test: /\.(png|jpg|gif)$/,
          use: [
            {
              loader: 'url-loader',
              options:{
                limite:8192
              }
            }
          ]
        }
    ]
  }
};

module.exports = config;
