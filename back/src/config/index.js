import os from 'os'
import crypto from 'crypto'
import cluster from 'express-cluster'
import path from 'path'
// import * as models from './models'
import * as classes from './classes'
import Database from './Database'

const db = new Database (  )

const mode = [
	true,
	{ "dev": true },
	{ "prod": false }
]

const port = 3000
const cores = os.cpus (  ).length - 1

const apiflag = '/api'

const versionApiFlag = '/v1'

const api = versionApiFlag + apiflag

const hash = ( secret ) => {

	return crypto.createHash ( 'sha256' )
	  .update ( secret )
	  .digest ( 'hex' )
}

module.exports = {
	/// tools
	port,
	mode,
	// models,
	classes,
	path,
	cluster,
	api,
	//// database instance
	db,
	//// crypto tools
	hash,
	crypto,
	//// tools core utils
	cores
}
