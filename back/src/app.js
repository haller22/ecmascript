import lib from './lib'
import config from './config'

config.cluster ( ( worker ) => {

	lib.app.listen ( config.port, (  ) => {

		console.log ( "Up Server in " + config.port )
	} )
}, { count : ( config.cores ) } )
