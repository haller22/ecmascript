module.exports = ( app ) => {

	app.get ( app.config.api + '/tests/:id', ( req, res, next ) => {

		const jwt = req.params.id

		let page = jwt
		res.send ( page )
	} )
	app.get ( app.config.api + '/tests', ( req, res, next ) => {

		let page = 'isValid'
		res.send ( page )
	} )
	// app.all( '/tests', ( req, res, next ) => {
	//   // runs for all HTTP verbs first
	//   // think of it as route specific middleware!
	// 	res.status( 404 ).send( '' )
	// } );

	return app
}
