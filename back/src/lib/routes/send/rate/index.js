module.exports = ( app ) => {

	// Add headers
	app.use ( (req, res, next) =>  {

	    res.setHeader ( 'Access-Control-Allow-Origin', '*' );

	    res.setHeader ( 'Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE' );

	    res.setHeader ( 'Access-Control-Allow-Headers', 'X-Requested-With,content-type' );

  		res.setHeader ( 'Access-Control-Allow-Credentials', true );

	    next (  );
	} );

	app.get ( app.config.api + '/rate/:id', ( req, res, next ) => {

		( new app.config.models.Cost (  ) ).search ( { "id": req.params.id }, ( resp ) => {

      res.send ( resp )
    } )
	} )

  app.delete ( app.config.api + '/rate/:id', ( req, res, next ) => {

  	( new app.config.models.Cost (  ) ).removeCost ( {
			"id": req.params.id
		}, ( resp ) => {

      res.send ( resp )
    } )
  } )

	app.get ( app.config.api + '/rate', ( req, res, next ) => {

		( new app.config.models.Cost (  ) ).show ( ( resp ) => {

      res.send ( resp )
    } )
	} )

	app.put ( app.config.api + '/rate/:id', ( req, res, next ) => {

		( new app.config.models.Cost (  ) ).update( {
        type: req.body.type,
        value: req.body.date,
      }, {
        email: req.body.emailBefore
      }, ( resp ) => {

        res.send ( resp )
      } )

	} )

	app.post ( app.config.api + '/rate', ( req, res, next ) => {

		let dataRequest = {
      type: req.body.type,
      value: req.body.date,
    }

		let rate = ( new app.config.models.Cost (  ) )

		let valueCheckFields = rate.check({valid: {
		      value: true,
		      data: dataRequest
		    }
		})

		if ( valueCheckFields.status ) {

   		rate.add( dataRequest, ( resp ) => {

        res.send ( resp )
      } )
		} else {

		  res.send ( {
				status: "error"
			} )
		}

	} )

	return app
}
